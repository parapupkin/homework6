﻿<?php 
if (!file_exists(__DIR__ . "/tests")) {
    mkdir(__DIR__ . "/tests");
}
if (!empty($_POST['bsubmit-pro'])) {   
} elseif (empty($_FILES['test']['name'])) {
    echo "Загрузите файл, прежде чем кликать по кнопке :)";
    exit();
} 
if(!empty($_FILES) && $_FILES['test']['error'] == 0) { 
// проверка на максимальный размер файла
$maxsize = 1000000; 
$size = filesize($_FILES['test']['tmp_name']);   
  if($size > $maxsize) {       
    echo "Для загрузки доступны файлы менее 5 Мб";  
    exit();     
  } 
// указываем папку для хранения тестов    
$destiation_dir = "tests" .'/'.$_FILES['test']['name'];
// запоминаем имя файла
$fileName = $_FILES['test']['name'];
//проверяем тип файла 
$info = new SplFileInfo($_FILES['test']['name']);
$infoType = $info->getExtension();
if ($infoType != "json") {
    echo "Для загрузки доступны только файлы json. Вернитесь на предыдущую страницу и загрузите корректный формат";
    exit();
}
// изменяем тип временного файла на json
$newType = stristr($_FILES['test']['tmp_name'], '.', true) . ".json";
$content_name = rename($_FILES['test']['tmp_name'], $newType); 
$content = file_get_contents($newType);
$result = json_decode($content, true);
//проверяем, чтобы там был массив
if (gettype($result) !== "array") {
    echo "Неверная структура json";
    unlink($newType);
    exit();
}
// Выполняем проверку структуры json
foreach ($result as $value) {    
   if (array_key_exists('question', $value) == 0 || array_key_exists('answers', $value) == 0 || array_key_exists('true', $value) == 0) {
    echo "Неверная структура json - не те ключи";
    unlink($newType);
    exit();
   } else {     
        if (gettype($value['answers']) !== "array") {
            echo "Неверная структура json";
            unlink($newType);
            exit();
        }      
  }    
}
//если все нормально - перемещаем файл и выдаем ссобщение о загрузке
rename($newType, "tests" .'/'. $fileName);
// move_uploaded_file($newType, $destiation_dir ); 
$message = 'Новый тест загружен <br>';
}
else {
$message = 'Новый тест не загружен'; // Оповещаем пользователя о том, что файл не был загружен
} 

// сканируем папку хранения тестов и выдаем список
$dir ='tests';
$list=scandir($dir);
$z=count(scandir('tests'))-1;
$x=2;
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Weather in <?= $name ?></title>
  <style type="text/css">
            html {
                background: #5C5B91;
            }
            form {
                margin-top: 20px;
                /*border: 2px solid;*/
                text-align: center;
                font-size: 20px;
                margin-bottom: 20px;
            }
            input {
                margin: 20px;
                font-size: 20px;
            }

            input:hover {
                border-color: #2A295F;
            }
            .container {
                text-align: center;
                font-size: 22px;
            }
            .container p {
                font-size: 22px;
                font-weight: bold;                
            }
            .message {
              color: white;
            }            
        </style>
</head>
<body>
  <div class="container">
    <p class="message"> <?php echo "$message";?></p>
    <p>Список загруженных тестов</p>
    <?php do {
              echo $x-1 . ". " . $list[$x] . "</br>";    
          }   while ($x++<$z);
    ?>
    <form enctype="multipart/form-data" action="test.php" method="get" >Введите номер теста   
        <input type="text" name="number" value="0" /><br/>                
        <button type="submit">Выбрать этот номер</button>    
    </form>
    <a href="admin.php">Загрузить еще один тест</a>
  </div>
</body>
</html>