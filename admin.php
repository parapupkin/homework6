<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Форма для передачи данных методом POST</title>
        <style type="text/css">
            html {
                background: #5C5B91;
            }
            form {
                margin-top: 20px;                
                text-align: center;
                font-size: 20px;
            }
            input {
                margin: 20px;
                font-size: 20px;
            }
            input:hover {
                border-color: #2A295F;
            }
            .container {
                text-align: center;
            }
            .container p {
                font-size: 22px;                
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Ну что, студент... готов? Сейчас мы тебя проверим!</h1>
            <p>Заполните поле с именем и отправьте файл теста</p>            
        </div>        
        <form enctype="multipart/form-data" action="list.php" method="post" >Введите имя   
            <input type="text" name="name" value="Василий Иванов" /><br/>
            <input type="file" name="test"><br/>            
            <input type="submit" name="bsubmit" value="Отправить" />                       
        </form>
        <form action="list.php" method="post" >
            <input type="submit" name="bsubmit-pro" value="Смотреть доступные тесты" />
        </form>
    </body>
</html>


