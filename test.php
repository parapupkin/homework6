﻿<?php

$number = ($_GET['number']);
$dir = (__DIR__ . "/tests/");
$list=scandir($dir);
if ($number < 1 || !ctype_digit($number) || $number > count($list) - 2) {
    echo "Вернитесь на предыдущую страницу и введите корректный номер теста";
    exit();
}
$test = $list[$number + 1];
$content = file_get_contents($dir. $test);
$result = json_decode($content, true);
$test_name = explode(".", $test);
$test_name = $test_name[0];
$i = 0;
$ot = 0;
$not = 0;
$not1 = 0;
$answers = [];
$mark;  

foreach ($result as $value) {      
    foreach ($value as $questions => $answer) {       
            $answers[$questions] = $answer;
    } 
    if (empty($_POST)) {        
    } elseif (empty($_POST["q$i"])) {
            $not1++;            
    } elseif ($_POST["q$i"] == $answers["true"]) {
            $ot++;            
    } else {
            $not++;            
    }
            $i++; 
}

if ($ot == $i) {
    $mark = "Отлично!";
} elseif ($ot == $i - 1) {
    $mark = "Хорошо";
} elseif ($ot == $i - 2) {
    $mark = "Удовлетворительно";
} else {
    $mark = "Плохо!";
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Test</title>
        <style type="text/css">
            body {
                line-height: 1.5;                
            }
            h1 {
                text-align: center;
                color: blue;
            }
            p {
              font-weight: bold;
            }
            .result {
                text-align: center;
                font-weight: bold;
                font-size: 20px;   
                color: red;     
            }            
            .result:nth-last-child(3) {
                color: green;
            }
            div {
                margin: 30px auto;
                width: 500px;
            }               
        </style>
    </head>
    <body>
        <h1>Вы проходите тест <?php echo "$test_name" ?> </h1>
        <form action="" method="POST">
            <div>
                <?php                 
                $i = 0;                  
                foreach ($result as $value) {      
                    foreach ($value as $questions => $answer) {       
                            $answers[$questions] = $answer;
                   }              
                ?>                    
                <p> <?php echo $answers["question"] ?></p>               
                <?php foreach ($answers['answers'] as $answer) { ?>
                <label><input  name="<?php echo "q" . $i?>" type="radio" value="<?php echo $answer ?>"> <?php echo $answer ?></label><br> 
                <?php }                 
                 $i++;                                    
                }
                ?>
                <p><label> Введите свое имя и фамилию <input  name="name" type="text" value="Неизвестный"></label></p> 
                <button class="button" type="submit">Результат</button>   
                <?php 
                if (!empty($_POST)) {
                echo  
                "<p class=\"result\"> Правильных ответов - " . $ot . "</p>" .
                "<p class=\"result\"> Неправильных ответов - " . $not . "</p>" .
                "<p class=\"result\"> Не отвечено - " . $not1 . "</p>";
                }  
                ?>                                        
            </div>
        </form>
    </body>
</html>

